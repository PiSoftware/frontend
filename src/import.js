import swal from 'sweetalert2';


export function importReport(response) {
    console.log("Imported");
  swal('Import Report', response.message).then(() => window.location.reload(true));
}
