import {call, put, takeEvery, takeLatest} from "redux-saga/effects"

import {REQUEST_ALL_DATA ,REQUEST_IMPORTED_DATA} from "../ActionTypes";

import {receiveAllData,receiveImportedData} from "../actions"

import {fetchData} from "../../services";


// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* getApiData(action) {
    try {
        // do api call
        const data = yield call(fetchData);
        yield put(receiveAllData(data));
    } catch (e) {
        console.log(e);
    }
}
function* getImportData(action) {
    try {
        // do api call
        const data = yield call(fetchData);
        yield put(receiveImportedData(data));
    } catch (e) {
        console.log(e);
    }
}

/*
  Alternatively you may use takeLatest.
  Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
  dispatched while a fetch is already pending, that pending fetch is cancelled
  and only the latest one will be run.
*/
export default function* mySaga() {
    yield takeLatest(REQUEST_ALL_DATA, getApiData);
    yield  takeLatest(REQUEST_IMPORTED_DATA,getImportData)
}