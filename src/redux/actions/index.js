import * as types from '../ActionTypes';


export const switchTab = activeTab => ({ type: types.SWITCH_TAB, activeTab });

export const requestAllData = () => ({ type: types.REQUEST_ALL_DATA });
export const receiveAllData = res => ({ type: types.RECEIVE_ALL_DATA, data: res.data });


export const requestImportedData = () => ({ type: types.REQUEST_IMPORTED_DATA });
export const receiveImportedData = res => ({ type: types.RECEIVE_IMPORTED_DATA, data: res.data.imported });
