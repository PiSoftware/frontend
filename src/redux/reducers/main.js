import {
    SWITCH_TAB,
     RECEIVE_ALL_DATA,RECEIVE_IMPORTED_DATA
} from '../ActionTypes'

const initialState =
    {
        tabs: {
            activeTab: 'claims',
        },
        data:{
            claims: [],
            members: [],
            projects: [],
            imported: [],
        }
    }
;

export function tabs(state = initialState.tabs, action) {
    switch (action.type) {
        case SWITCH_TAB:
            return {
                ...state,
                activeTab: action.activeTab,
            };
        default:
            return state
    }
}


export function data(state = initialState.data, action) {
    switch (action.type) {
        case RECEIVE_ALL_DATA:
            return {
                ...state,
                ...action.data
            };
        case RECEIVE_IMPORTED_DATA:
            return {
                ...state,
                imported: action.data
            };
        default:
            return state
    }
}