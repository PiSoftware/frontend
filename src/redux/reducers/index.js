import {combineReducers} from 'redux'
import {tabs,data} from './main';

const rootReducer = combineReducers({
    tabs,data
});

export default rootReducer
