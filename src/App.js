import React, {Component} from 'react';
import './App.css';
import 'antd/dist/antd.css';
import connect from 'react-redux/es/connect/connect';
import MyMenu from './Menu/MyMenu';
import ListClaims from './Pages/ListClaims';
import AddClaim from './Pages/AddClaim';
import ListMembers from './Pages/ListMembers';
import AddMember from './Pages/AddMember';
import ListProjects from './Pages/ListProjects';
import AddProject from './Pages/AddProject';
import ImportDiff from './Pages/ImportDiff';
import {BaseURL} from './Environment';


import {switchTab, requestAllData} from './redux/actions';
import {
    getActiveTab, getClaims, getImported, getMembers, getProjects,
} from './redux/selectors';


export class App extends Component {
    constructor(props) {
        super(props);
        this.state = {

            selectedClaim: [],
            selectedProject: [],
            selectedMember: [],
            selectedClaimMember: [],
            selectedClaimProject: [],

            isClaimMultiAddable: 'multiple',
            distinctProjects: [],

            defaultFilterFullName: [],
        };
        this.changeCurrentMenuTab = this.changeCurrentMenuTab.bind(this);
        this.handleClaimMemberChange = this.handleClaimMemberChange.bind(this);
        this.handleClaimProjectChange = this.handleClaimProjectChange.bind(this);
        this.handleOnRowUpdate = this.handleOnRowUpdate.bind(this);
        this.handleMemberChange = this.handleMemberChange.bind(this);
        this.handleProjectChange = this.handleProjectChange.bind(this);

        this.onProjectSave = this.onProjectSave.bind(this);
        this.onMemberSave = this.onMemberSave.bind(this);
        this.changeIsClaimMultiAddable = this.changeIsClaimMultiAddable.bind(this);
        this.handleClaimCancel = this.handleClaimCancel.bind(this);

    }

    handleClaimMemberChange(value) {
        if (value != null) {
            if (this.state.isClaimMultiAddable === 'default') { // if dropdown selection is not multiple, value is string and not in array.
                const convertStringToArray = []; // to make map function working, string needs to be in array.
                convertStringToArray.push(value);
                value = convertStringToArray;
            }

            const temp = [];
            value.map(values => temp.push(this.state.members.find(e => e.id === values)));

            this.setState({
                selectedClaimMember: temp,
            });
        } else {
            this.setState({
                selectedClaimMember: [],
            });
        }
    }

    handleClaimProjectChange(value) {
        if (value != null) {
            this.setState({
                selectedClaimProject: this.state.projects.find(e => e.projectId === value),
            });
        } else {
            this.setState({
                selectedClaimProject: [],
            });
        }
    }

    changeIsClaimMultiAddable(value) {
        this.setState({
            isClaimMultiAddable: value ? 'multiple' : 'default',
        });
    }

    handleMemberChange(value) {
        if (value != null) {
            this.setState({
                selectedMember: value,
            });
        } else {
            this.setState({
                selectedMember: [],
            });
        }
    }

    handleProjectChange(value) {
        if (value != null) {
            this.setState({
                selectedProject: value,
            });
        } else {
            this.setState({
                selectedProject: [],
            });
        }
    }

    componentWillMount() {
        this.props.requestAllData();
    }

    handleClaimCancel() {
        this.setState({
            current: 'claims',
        });
    }

    onProjectSave() {
        this.getProjects();
        this.getClaims();
    }

    onMemberSave() {
        this.getMembers();
        this.getClaims();
    }

    handleOnRowUpdate(value) {
        if (value != null) {
            this.setState({
                selectedClaim: value,
            });
        } else {
            this.setState({
                selectedClaim: [],
            });
        }
    }


    changeCurrentMenuTab(newCurrent) {
        this.props.switchTab(newCurrent);
    }


    defaultFilterFullNameHandler = (val) => {
        this.setState({
            defaultFilterFullName: [val]
        })
    };


    render() {
        return (
            <div className="body">

                <MyMenu current={this.props.getActiveTab} onMenuClick={this.changeCurrentMenuTab}/>
                {this.props.getActiveTab === 'claims'
                    ? (
                        <ListClaims
                            totalClaims={this.state.totalClaims}
                            claims={this.props.getClaims}
                            projects={this.props.getProjects}
                            handleOnRowUpdate={this.handleOnRowUpdate}
                            onClaimDelete={this.props.requestAllData}
                            changeIsClaimMultiAddable={this.changeIsClaimMultiAddable}
                            defaultFilterFullName={this.state.defaultFilterFullName}
                            defaultFilterFullNameHandler={this.defaultFilterFullNameHandler}/>
                    ) : null}

                {this.props.getActiveTab === 'claim-add'
                    ? (
                        <AddClaim
                            members={this.props.getMembers}
                            projects={this.props.getProjects}
                            selectedClaim={this.state.selectedClaim}
                            updateClaims={this.props.requestAllData}
                            resetSelectedClaim={this.handleOnRowUpdate}
                            handleClaimMemberChange={this.handleClaimMemberChange}
                            handleClaimProjectChange={this.handleClaimProjectChange}

                            changeIsClaimMultiAddable={this.changeIsClaimMultiAddable}
                            isClaimMultiAddable={this.state.isClaimMultiAddable}
                            cancelClaim={this.handleClaimCancel}
                        />
                    ) : null}
                {this.props.getActiveTab === 'members'
                    ? (
                        <ListMembers
                            onMemberDelete={this.props.requestAllData}
                            members={this.props.getMembers}
                            handleUpdateClick={this.handleMemberChange}
                        />
                    ) : null}
                {this.props.getActiveTab === 'member-add'
                    ? (
                        <AddMember
                            resetSelectedMemberState={this.handleMemberChange}
                            onSave={this.props.requestAllData}
                            selectedMember={this.state.selectedMember}
                        />
                    ) : null}
                {this.props.getActiveTab === 'projects'
                    ? (
                        <ListProjects
                            onProjectDelete={this.props.requestAllData}
                            projects={this.props.getProjects}
                            handleUpdateClick={this.handleProjectChange}
                        />
                    ) : null}
                {this.props.getActiveTab === 'project-add'
                    ? (
                        <AddProject
                            onSave={this.props.requestAllData}
                            members={this.props.getMembers}
                            resetSelectedProjectState={this.handleProjectChange}
                            selectedProject={this.state.selectedProject}
                        />
                    ) : null}
                {this.props.getActiveTab === 'imported'
                    ? (
                        <ImportDiff imports={this.props.getImported}/>
                    ) : null}


            </div>
        );
    }
}

const mapStateToProps = state => ({
    getActiveTab: getActiveTab(state),
    getClaims: getClaims(state),
    getMembers: getMembers(state),
    getProjects: getProjects(state),
    getImported: getImported(state),
});
const mapDispatchToProps = {switchTab, requestAllData};
export default connect(mapStateToProps, mapDispatchToProps)(App);
