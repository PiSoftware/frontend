/* eslint-disable no-unused-vars,no-undef */
import { write as xlsxWrite, utils as xlsxUtils } from 'xlsx';
import { saveAs } from 'file-saver';
import { BaseURL } from './Environment';

function dateConverter(date) {
  const yyyy = date.getFullYear();
  const mm = date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  // getMonth() is zero-based
  const dd = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  const hh = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
  const min = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
  return ''
    .concat(yyyy)
    .concat(mm)
    .concat(dd)
    .concat(hh)
    .concat(min);
}

function adjustFieldsForExcel(dataItems) {
  return dataItems.map(item => ({
    Week: item.week,
    Date: item.date,
    'Member ID': item.memberid,
    'Member Name': item.memberfullname,
    'Member Team': item.memberteam,
    'Site Name': item.projectname,
    'Project Name': item.projectnameofsite,
    Phm: item.phm,
    Pmd: item.pmd,
    Amh: item.amh,
    'Transfer By': item.transferby,
    Status: item.status,
    Tracker: item.tracker,
    Remark: item.remark,
  }));
}

function createWorkSheet(data) {
  return xlsxUtils.json_to_sheet(data);
}

function createWorkbook() {
  return xlsxUtils.book_new();
}

function addSheetToWorkbook(workBook, workSheet) {
  xlsxUtils.book_append_sheet(workBook, workSheet);
}

export function createBlobFromWorkBook(workBook) {
  /* bookType can be any supported output type */
  const wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };
  const wbout = xlsxWrite(workBook, wopts);

  function s2ab(s) {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    // eslint-disable-next-line no-plusplus,no-bitwise
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }

  return new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
}

export function exportExcel(dataItems, ttID, option) {
  window.open(`${BaseURL}export`, '_blank');
}

export function downloadAreaImportTemplate() {
  const fields = [
    { SiteName: 'Some_Site_1', AccessType: 'Access Type(2G or 3G or 4G)' },
    { SiteName: 'Some_Site_2', AccessType: 'Access Type(2G or 3G or 4G)' },
  ];
  const workSheet = createWorkSheet(fields);
  const workBook = createWorkbook();
  addSheetToWorkbook(workBook, workSheet);
  const xlsBlob = createBlobFromWorkBook(workBook);
  saveAs(xlsBlob, 'Outage_Dashboard_Area_Import_Template.xlsx');
}
