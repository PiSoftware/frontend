import React, {Component} from 'react';
import {Input, Button} from 'antd';
import swal from 'sweetalert2';
import connect from 'react-redux/es/connect/connect';
import {BaseURL} from '../Environment';
import {requestAllData, switchTab} from '../redux/actions';


const initialState = {
    member: {id: '', fullName: '', team: ''},
    isIdDisabled: false,
};

class AddMember extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.onChangeMemberId = this.onChangeMemberId.bind(this);
        this.onChangeMemberName = this.onChangeMemberName.bind(this);
        this.onChangeMemberTeam = this.onChangeMemberTeam.bind(this);

        this.onMemberSave = this.onMemberSave.bind(this);
        this.createMember = this.createMember.bind(this);
        this.clearStatesMember = this.clearStatesMember.bind(this);
    }

    clearStatesMember() {
        this.setState(initialState);
        this.props.resetSelectedMemberState();
    }

    onMemberSave() {
        if (this.props.selectedMember === undefined || this.props.selectedMember.length == 0) {
            this.createMember();// Post()
        } else {
            this.editMember();// Put
        }
        this.props.switchTab('members');
    }

    createMember() {
        fetch(`${BaseURL}members`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.member),
        }).then((response) => {
            if (response.status === 200) {
                swal('Added Succesfully', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
                this.props.onSave();
                this.clearStatesMember();
                response.json().then((data) => {
                    console.log(data);
                });
            } else {
                swal('Could not add Member. Please check the fields', {
                    icon: 'error',
                    buttons: false,
                    timer: 1500,
                });
            }
        });
    }

    editMember() {
        const urlParam = this.props.selectedMember.id;
        fetch(`${BaseURL}members/${urlParam}`, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.member),
        }).then((response) => {
            if (response.status === 200) {
                swal('Updated Succesfully', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
                this.props.onSave();
                this.props.resetSelectedMemberState();
                this.clearStatesMember();
                response.json().then((data) => {
                    console.log(data);
                });
            } else {
                swal('Could not update Member. Please check the fields', {
                    icon: 'error',
                    buttons: false,
                    timer: 1500,
                });
            }
        });
    }

    onChangeMemberId(e) {
        if (!(e.target.value.search(/^0/) !== -1) && !(e.target.value.length > 6)) {
            const val = e.target.value.replace(/\D/g, '');
            this.setState(prevState => ({
                member: {...prevState.member, id: val},
            }));
        }
    }

    onChangeMemberName(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                member: {...prevState.member, fullName: val},
            })
        );
    }

    onChangeMemberTeam(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                member: {...prevState.member, team: val},
            })
        );
    }

    componentWillMount() {
        this.setState({
            member: {
                id: this.props.selectedMember.id,
                fullName: this.props.selectedMember.fullName,
                team: this.props.selectedMember.team
            }
        });

        if (!(this.props.selectedMember === undefined || this.props.selectedMember.length === 0)) {
            this.setState({
                isIdDisabled: true,
            });
        }
    }

    render() {
        return (
            <div>
                <div className="add-member-container">
                    <h4>ID</h4>
                    <Input
                        disabled={this.state.isIdDisabled}
                        placeholder="ID"
                        value={this.state.member.id}
                        onChange={this.onChangeMemberId}
                        style={{width: 200}}
                    />
                    <br/>
                    <h4>Name</h4>
                    <Input
                        placeholder="Full Name"
                        value={this.state.member.fullName}
                        onChange={this.onChangeMemberName}
                        style={{width: 200}}
                    />
                    <br/>
                    <h4>Team</h4>
                    <Input
                        placeholder="Team"
                        value={this.state.member.team}
                        onChange={this.onChangeMemberTeam}
                        style={{width: 200}}
                    />
                    <br/>
                    <div className="add-member-buttons" style={{width: 200}}>
                        <Button
                            type="primary"
                            style={{float: 'left'}}
                            icon="save"
                            onClick={this.onMemberSave}
                        >Save</Button>
                        <Button
                            ghost
                            type="primary"
                            style={{float: 'right'}}
                            icon="delete"
                            onClick={this.clearStatesMember}
                        >Clear</Button>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = state => ({});
const mapDispatchToProps = {switchTab, requestAllData};
export default connect(mapStateToProps, mapDispatchToProps)(AddMember);
