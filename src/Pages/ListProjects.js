import React, {Component} from 'react';
import {Table} from 'antd';
import {Divider} from 'antd';
import {BaseURL} from '../Environment';
import {switchTab} from "../redux/actions";
import connect from "react-redux/es/connect/connect";
import {deleteConfirm, deleteConfirmation} from "../utils";


 class ListProjects extends Component {

    constructor(props) {
        super(props);
        this.updateProject = this.updateProject.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
    }

    updateProject(value) {
        this.props.handleUpdateClick(value);
        this.props.switchTab("project-add");
    }


     deleteProject(record, index) {
         const onProjectDelete = this.props.onProjectDelete;
         deleteConfirmation(() => {
             fetch(`${BaseURL}projects/delete/${record.id}`, {
                 method: 'put',
             }).then(response => {
                 onProjectDelete();
             });
         })
     }


    render() {
        const columnsProjects = [
            {
                title: 'Site Name',
                dataIndex: 'siteName',
            }, {
                title: 'Project Code',
                dataIndex: 'projectCode',
            }, {
                title: 'Sub-Project Code',
                dataIndex: 'subProjectCode',
            }, {
                title: 'Project Type',
                dataIndex: 'projectType',
            }, {
                title: 'Project Level',
                dataIndex: 'projectLevel',
            }, {
                title: 'Project Name',
                dataIndex: 'projectName',
            }, {
                title: 'Tracker',
                dataIndex: 'tracker.fullName',
            }, {
                title: 'Approver',
                dataIndex: 'approver',
            }, {
                title: 'Action', dataIndex: '', key: 'x',
                render: (record, index) =>
                    <div>
                        <a href="javascript:;" onClick={() => this.updateProject(record, index)}>Update</a>
                        <Divider type="vertical"/>
                        <a href="javascript:;" onClick={() => this.deleteProject(record, index)}>Delete</a>
                    </div>
            }];

        return (
            <div className="projects">
                <h2> Projects </h2>
                <Table dataSource={this.props.projects}
                       columns={columnsProjects}/>
            </div>
        );
    }
}



const mapStateToProps = state => ({});
const mapDispatchToProps = {switchTab };
export default connect(mapStateToProps, mapDispatchToProps)(ListProjects);