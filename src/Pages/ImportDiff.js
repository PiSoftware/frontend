import React, {Component} from 'react';
import {
    Button, Divider, Table,
} from 'antd';

import connect from 'react-redux/es/connect/connect';

import {switchTab} from '../redux/actions';
import {FilterWeeks} from '../Constants';


class ImportDiff extends Component {
    render() {
        const columns = [
            {
                title: 'Import Status',
                dataIndex: 'importStatus',
            },
            {
                title: 'Week',
                dataIndex: 'week',
            }, {
                title: 'Date',
                dataIndex: 'date',
            }, {
                title: 'Member id',
                dataIndex: 'member.id',

            }, {
                title: 'Full Name',
                dataIndex: 'member.fullName',
            }, {
                title: 'Team',
                dataIndex: 'member.team',
            }, {
                title: 'Site name',
                dataIndex: 'project.siteName',
            }, {
                title: 'ProjectName',
                dataIndex: 'project.projectName',
            },
            {
                title: 'PHM',
                dataIndex: 'status.phm',
            },
            {
                title: 'PMD',
                dataIndex: 'status.pmd',
            },
            {
                title: 'AMH',
                dataIndex: 'status.amh',
            }, {
                title: 'Transfer By',
                dataIndex: 'status.transferBy',
            }, {
                title: 'Status',
                dataIndex: 'status.status',

            }, {
                title: 'Tracker',
                dataIndex: 'status.tracker.fullName',
            }, {
                title: 'Remark',
                dataIndex: 'status.remark',
                width: 200,
            },
        ];
        return (
            <div>
                <Table
                    pagination={{pageSize: 20}}
                    dataSource={this.props.imports}
                    columns={columns}
                    size="small"
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = {switchTab};
export default connect(mapStateToProps, mapDispatchToProps)(ImportDiff);
