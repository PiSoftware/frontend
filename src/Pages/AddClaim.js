import React, {Component} from 'react';
import {
    Select, Input, InputNumber, Button,
} from 'antd';
import moment from 'moment';
import swal from 'sweetalert2';
import {Weeks} from '../Constants';
import {BaseURL} from '../Environment';
import {setDateInterval} from "../utils";

const Option = Select.Option;
const {TextArea} = Input;

const initialState = {
    claim: {
        id: null,
        week: moment().week().toString(),
        date: setDateInterval(moment().week().toString()),
        approvedDate: '',
        member: {id: []},
        project: {id: []},
        status: {id: [], tracker: {id: []}, remark: '', status: [], phm: 1, pmd: 1, transferBy: "times"},
    },

    projectDropdownState: [],
};

class AddClaim extends Component {
    constructor(props) {
        super(props);

        this.state = initialState;
        this.handleProjectChange = this.handleProjectChange.bind(this);
        this.handleMemberChange = this.handleMemberChange.bind(this);
        this.handleTrackerChange = this.handleTrackerChange.bind(this);
        this.handleTransferByChange = this.handleTransferByChange.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this);
        this.handleRemarkChange = this.handleRemarkChange.bind(this);
        this.handleDaysChange = this.handleDaysChange.bind(this);
        this.handleHoursChange = this.handleHoursChange.bind(this);
        this.handleWeekChange = this.handleWeekChange.bind(this);
        this.createClaim = this.createClaim.bind(this);
        this.editClaim = this.editClaim.bind(this);
        this.handleClaimSave = this.handleClaimSave.bind(this);
        this.cancelClaim = this.cancelClaim.bind(this);
        this.clearStates = this.clearStates.bind(this);
    }

    componentWillMount() {
        if (this.props.selectedClaim.length === 0) {
            return;
        }
        console.log(this.props.selectedClaim)
        this.setState({
            claim: {
                id: this.props.selectedClaim.id,
                week: "W" + this.props.selectedClaim.week,
                date: this.props.selectedClaim.date,
                approvedDate: '',
                member: {id: this.props.selectedClaim.member.id},
                project: {id: this.props.selectedClaim.project.id},
                status: {
                    id: this.props.selectedClaim.status.id,
                    tracker: {id: this.props.selectedClaim.status.tracker.id},
                    remark: this.props.selectedClaim.status.remark,
                    status: this.props.selectedClaim.status.status,
                    phm: this.props.selectedClaim.status.phm,
                    pmd: this.props.selectedClaim.status.pmd,
                    transferBy: this.props.selectedClaim.status.transferBy
                },
            },
        });
    }


    handleMemberChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, member: {id: val}},
            })
        );
    }

    handleDaysChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, pmd: val}},
            })
        );
    }

    handleHoursChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, phm: val}},
            })
        );
    }

    handleTrackerChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, tracker: {id: val}}},
            })
        );
    }

    handleWeekChange(value) {
        const val = value;
        this.setState(prevState => ({
            claim: {...prevState.claim, week: val},
        }), () => {
            let setDateValue; // if user clears all week field, give actual week interval to date, else take last week input
            if (!(this.state.claim.week === undefined || this.state.claim.week.length === 0)) {
                setDateValue = this.state.claim.week[this.state.claim.week.length - 1];
            } else setDateValue = moment().week();
            this.setState(prevState => ({
                claim: {...prevState.claim, date: setDateInterval(setDateValue)},
            }));
        });
    }


    handleProjectChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, project: {id: val}},
            })
        );
    }

    handleTransferByChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, transferBy: val}},
            })
        );
    }

    handleStatusChange(value) {
        const val = value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, status: val}},
            })
        );
    }

    handleRemarkChange(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                claim: {...prevState.claim, status: {...prevState.claim.status, remark: val}},
            })
        );
    }

    createClaim() {
        fetch(`${BaseURL}claims/*`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.claim),
        }).then(response => response.json()).then((jsonResponse) => {
                if (jsonResponse.status === 500) {
                    jsonResponse.type = "error";
                    jsonResponse.title = "Please fill reqiured fields !";
                    jsonResponse.message = null;
                }
                swal({
                    type: jsonResponse.type,
                    title: jsonResponse.title,
                    text: jsonResponse.message,
                    buttons: false,
                });
                if (jsonResponse.type === "success") {
                    this.clearStates();
                    this.props.updateClaims();
                }
            }
        )
        ;
    }

    editClaim() {
        const urlparamClaimId = this.props.selectedClaim.id;
        const urlparamStatusId = this.props.selectedClaim.status.id;
        fetch(`${BaseURL}claims/${urlparamClaimId}/status/${urlparamStatusId}`, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.claim),
        }).then((response) => {
            if (response.status === 200) {
                swal('Updated Succesfully', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
                this.clearStates();
                this.props.updateClaims();
                response.json().then((data) => {
                    // console.log(data);
                });
            } else {
                swal('Could not update the Claim. Please Check the fields', {
                    icon: 'error',
                    buttons: false,
                    timer: 1500,
                });
            }
        });
    }

    cancelClaim() {
        this.clearStates();
        this.props.cancelClaim();
    }

    clearStates() {
        this.setState(initialState);

        this.props.resetSelectedClaim();
        this.props.handleClaimMemberChange();
        this.props.changeIsClaimMultiAddable(true);
        // same
    }

    validateInputs(){
        let requiredFields = [];
        if(!this.state.claim.status.tracker.id || this.state.claim.status.tracker.id===null){requiredFields.push("Tracker")}
        if(!this.state.claim.project.id || this.state.claim.project.id===null){requiredFields.push("Project")}
        if(this.state.claim.member.id.length===0 || !this.state.claim.member.id || this.state.claim.member.id===null){requiredFields.push("Member")}

        if(requiredFields.length>0){
        swal({
            title: "Missing Fields",
            text: "Please Check the fields: "+requiredFields.concat(),
            type: 'error',
        });
        }
        return requiredFields.length===0;
    }
    handleClaimSave() {
        if(!this.validateInputs()) return;
        if (this.props.selectedClaim === undefined || this.props.selectedClaim.length === 0) {
            this.createClaim();
        } else {
            this.editClaim();
        }
    }

    render() {
        return (
            <div className="main-wrapper">
                <div className="container">
                    <h4>Week</h4>
                    <Select
                        className="form-input"
                        style={{width: '100%'}}
                        mode={'multiple'}
                        value={this.state.claim.week}
                        showSearch
                        placeholder="Select week"
                        optionFilterProp="children"
                        onChange={this.handleWeekChange}
                    >
                        {Weeks.map(weeks => (
                            <Option key={weeks.key}>
                                {weeks.value}
                            </Option>
                        ))}

                    </Select>

                    <h4>Date</h4>
                    <Input
                        style={{width: '100%'}}
                        className="form-input"
                        disabled
                        placeholder="Date"
                        value={this.state.claim.date}
                    />

                    <h4>Project</h4>
                    <Select
                        className="form-input"
                        style={{width: '100%'}}
                        value={this.state.claim.project.id}
                        showSearch
                        placeholder="Select a project"
                        optionFilterProp="children"
                        onChange={this.handleProjectChange}
                    >
                        {this.props.projects.map(project => (
                            <Option
                                key={project.id}
                                value={project.id}
                            >
                                {`[${project.projectCode}]  ${project.projectName}`}
                            </Option>
                        ))}

                    </Select>

                    <h4>Member</h4>
                    <Select
                        mode={'multiple'}
                        className="form-input"
                        style={{width: '100%'}}
                        value={this.state.claim.member.id}
                        showSearch
                        placeholder="Select a member"
                        optionFilterProp="children"
                        onChange={this.handleMemberChange}
                    >
                        {this.props.members.map(member => (
                            <Option
                                key={member.id}
                                value={member.id}
                            >
                                {`${member.id} - ${member.fullName}`}
                            </Option>
                        ))}

                    </Select>

                    <h4>Tracker</h4>
                    <Select
                        className="form-input"
                        value={this.state.claim.status.tracker.id}
                        showSearch
                        style={{width: '100%'}}
                        placeholder="Select a Tracker"
                        optionFilterProp="children"
                        onChange={this.handleTrackerChange}
                    >
                        {this.props.members.map(member => (
                            <Option
                                key={member.id}
                                value={member.id}
                            >
                                {`${member.id} - ${member.fullName}`}
                            </Option>
                        ))}

                    </Select>

                    <h4>TransferBy</h4>
                    <Select
                        className="form-input"
                        value={this.state.claim.status.transferBy}
                        style={{width: '100%'}}
                        onChange={this.handleTransferByChange}
                    >
                        <Option value="times">
                            TIMES
                        </Option>
                        <Option value="x-change">
                            x-Charge
                        </Option>
                        <Option value="blanks">
                            Blanks
                        </Option>
                    </Select>

                    <h4>Status</h4>
                    <Select
                        className="form-input"
                        value={this.state.claim.status.status}
                        style={{width: '100%'}}
                        onChange={this.handleStatusChange}
                        placeholder="Select a Status"
                    >
                        <Option value="Approved">
                            Approved
                        </Option>
                        <Option value="Claimed">
                            Claimed
                        </Option>
                        <Option value="Not Approved">
                            Not Approved
                        </Option>
                        <Option value="Rejected">
                            Rejected
                        </Option>
                        <Option value="Waiting for Cost Agreement">
                            Waiting for Cost Agreement
                        </Option>
                        <Option value="Waiting for Claiming">
                            Waiting for Claiming
                        </Option>
                    </Select>

                    <h4>Remark</h4>
                    <TextArea
                        className="form-input"
                        value={this.state.claim.status.remark}
                        onChange={this.handleRemarkChange}
                        rows={2}
                        style={{width: '100%'}}
                    />
                </div>

                <div className="week-day-container" style={{width: '100%'}}>
                    <div className="left">
                        <h4 style={{width: 150}}>Days</h4>

                        <InputNumber
                            style={{width: 150}}
                            className="form-input"
                            min={1}
                            max={7}
                            defaultValue={5}
                            value={this.state.claim.status.pmd}
                            onChange={this.handleDaysChange}
                        />
                    </div>

                    <div className="right">
                        <h4 style={{width: 150}}>Hours</h4>
                        <InputNumber
                            style={{width: 150}}
                            className="form-input"
                            min={1}
                            max={8}
                            defaultValue={8}
                            value={this.state.claim.status.phm}
                            onChange={this.handleHoursChange}
                        />
                    </div>
                </div>

                <br/>

                <div className="btn-container" style={{width: '100%'}}>

                    <Button type="primary" icon="save" onClick={this.handleClaimSave}>Save</Button>
                    <Button ghost type="primary" icon="delete" onClick={this.cancelClaim}>Clear</Button>

                </div>
            </div>
        );
    }
}

export default AddClaim;