import React, {Component} from 'react';
import {Table} from 'antd';
import {Divider} from 'antd';
import {BaseURL} from '../Environment';
import {requestAllData, requestImportedData, switchTab} from "../redux/actions";
import connect from "react-redux/es/connect/connect";
import {deleteConfirm, deleteConfirmation} from "../utils";


class ListMembers extends Component {

    constructor(props) {
        super(props);

        this.updateMember = this.updateMember.bind(this);
        this.deleteMember = this.deleteMember.bind(this);
    }

    updateMember(value) {
        this.props.handleUpdateClick(value);
        this.props.switchTab("member-add");
    }

    deleteMember(record, index) {
        const onMemberDelete = this.props.onMemberDelete;
        deleteConfirmation(() => {
            fetch(`${BaseURL}members/delete/${record.id}`, {
                method: 'put',
            }).then(response => {
                onMemberDelete();
            });
        })
    }

    render() {
        const columnsMembers = [
            {
                title: 'ID',
                dataIndex: 'id',
            },
            {
                title: 'Member Name',
                dataIndex: 'fullName',
            },
            {
                title: 'Team',
                dataIndex: 'team',
            }, {
                title: 'Action', dataIndex: '', key: 'x',
                render: (record, index) =>
                    <div>
                        <a href="javascript:;" onClick={() => this.updateMember(record, index)}>Update</a>
                        <Divider type="vertical"/>
                        <a href="javascript:;" onClick={() => this.deleteMember(record, index)}>Delete</a>
                    </div>
            }];
        return (
            <div className="members">

                <h2> Members </h2>
                <Table dataSource={this.props.members}
                       columns={columnsMembers}/>
            </div>

        );
    }
}


const mapStateToProps = state => ({});
const mapDispatchToProps = {switchTab};
export default connect(mapStateToProps, mapDispatchToProps)(ListMembers);