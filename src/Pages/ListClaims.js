import React, {Component} from 'react';
import {Table, Divider, Input, Button, Icon, Upload} from 'antd';
import {FilterWeeks} from '../Constants';
import {BaseURL} from '../Environment';
import {exportExcel} from '../export';
import {requestAllData, requestImportedData, switchTab} from '../redux/actions'
import connect from "react-redux/es/connect/connect";
import {deleteConfirmation} from "../utils";
import swal from "sweetalert2";


const initialState = ({claims, projects}) => ({
    filterDropdownVisible: false,
    filterDropdownVisible_: false,
    claims: claims,
    projects: projects,
    isImporting: false,

    filtered: false,
    filtersSiteNames: [],
    filtersProjectNames: [],
    filtersProjectCodes: [],
});

class ListClaims extends Component {

    constructor(props) {
        super(props);

        this.state = initialState({claims: this.props.claims, projects: this.props.projects});

    }

    componentWillMount() {
        const filtersSiteNames = [...new Set(this.props.projects.map(project => project.siteName))].map(a=>({text:a,value:a}));
        const filtersProjectNames = [...new Set(this.props.projects.map(project => project.projectName))].map(a=>({text:a,value:a}));
        const filtersProjectCodes = [...new Set(this.props.projects.map(project => project.projectCode))].map(a=>({text:a,value:a}));

        this.setState({
            filtersSiteNames: filtersSiteNames,
            filtersProjectNames: filtersProjectNames,
            filtersProjectCodes: filtersProjectCodes,
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps !== this.props) {
            const filtersSiteNames = [...new Set(this.props.projects.map(project => project.siteName))].map(a=>({text:a,value:a}));
            const filtersProjectNames = [...new Set(this.props.projects.map(project => project.projectName))].map(a=>({text:a,value:a}));
            const filtersProjectCodes = [...new Set(this.props.projects.map(project => project.projectCode))].map(a=>({text:a,value:a}));

            this.setState({
                filtersSiteNames: filtersSiteNames,
                filtersProjectNames: filtersProjectNames,
                filtersProjectCodes: filtersProjectCodes,
            })
        }
    }


    updateClaim(record, index) {
        this.props.handleOnRowUpdate(record);
        this.props.changeIsClaimMultiAddable(false);
        this.props.switchTab("claim-add");
    }

    deleteClaim(record, index) {
        const onClaimDelete = this.props.onClaimDelete;
        deleteConfirmation(() => {
            fetch(`${BaseURL}claims/${record.id}`, {
                method: 'delete',
            }).then(response => {
                onClaimDelete();
            });
        })
    }

    importExcelStarting = () => {
        this.setState({isImporting: true});
    };
    importExcel = (jsonResponse) => {
        this.setState({isImporting: false});
        swal({
            type: jsonResponse.type,
            title: jsonResponse.title,
            text: jsonResponse.message,
            buttons: false,
        });
        this.props.switchTab("imported");
        this.props.requestAllData();
    };


    filterFullNameSearch = (selectedKeys, confirm) => () => {
        confirm();
        this.setState({filterFullNameSearch: selectedKeys[0]});
        this.props.defaultFilterFullNameHandler(selectedKeys[0]);
    };
    filterFullNameReset = clearFilters => () => {
        clearFilters();
        this.setState({filterFullNameSearch: ''});
    };
    filterTrackerSearch = (selectedKeys, confirm) => () => {
        confirm();
        this.setState({filterTrackerSearch: selectedKeys[0]});
    };
    filterTrackerReset = clearFilters => () => {
        clearFilters();
        this.setState({filterTrackerSearch: ''});
    };

    render() {
        const columns = [
            {
                title: 'Week',
                dataIndex: 'week', filters: FilterWeeks,
                onFilter: (value, record) => "W" + record.week === value,
            }, {
                title: 'Date',
                dataIndex: 'date',
            }, {
                title: 'Member id',
                dataIndex: 'member.id',

            }, {
                title: 'Full Name',
                dataIndex: 'member.fullName',
                filteredValue: this.props.defaultFilterFullName || null,
                filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => this.searchInput = ele}
                            placeholder="Search name"
                            value={selectedKeys[0]}
                            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                            onPressEnter={this.filterFullNameSearch(selectedKeys, confirm)}
                        />
                        <Button type="primary"
                                onClick={this.filterFullNameSearch(selectedKeys, confirm)}>Search</Button>
                        <Button onClick={this.filterFullNameReset(clearFilters)}>Reset</Button>
                    </div>
                ),
                filterIcon: filtered => <Icon type="search" style={{color: filtered ? '#108ee9' : '#aaa'}}/>,
                onFilter: (value, record) => record.member.fullName.toLowerCase().includes(value.toLowerCase()),
                onFilterDropdownVisibleChange: (visible) => {
                    if (visible) {
                        setTimeout(() => {
                            this.searchInput.focus();
                        });
                    }
                },
            }, {
                title: 'Site name',
                dataIndex: 'project.siteName',
                filters: this.state.filtersSiteNames,
                onFilter: (value, record) => {
                    return record.project.siteName === value
                }
            }, {
                title: 'ProjectCode',
                dataIndex: 'project.projectCode',
                filters: this.state.filtersProjectCodes,
                onFilter: (value, record) => {
                    return record.project.projectCode === value
                }
            }, {
                title: 'ProjectName',
                dataIndex: 'project.projectName',
                filters: this.state.filtersProjectNames,
                onFilter: (value, record) => {
                    return record.project.projectName === value
                }
            }, {
                title: 'PHM',
                dataIndex: 'status.phm',
            },
            {
                title: 'PMD',
                dataIndex: 'status.pmd',
            },
            {
                title: 'AMH',
                dataIndex: 'status.amh',
            }, {
                title: 'Transfer By',
                dataIndex: 'status.transferBy',
                filters: [{
                    text: 'times',
                    value: 'times',
                }, {
                    text: 'x-change',
                    value: 'x-change',
                },
                    {
                        text: 'blanks',
                        value: 'blanks',
                    }],
                onFilter: (value, record) => record.status.transferBy === value,

            }, {
                title: 'Status',
                dataIndex: 'status.status',

                filters: [{
                    text: 'Approved',
                    value: 'Approved',
                }, {
                    text: 'Not Approved',
                    value: 'Not Approved',
                },
                    {
                        text: 'Claimed',
                        value: 'Claimed',
                    },
                    {
                        text: 'Rejected',
                        value: 'Rejected',
                    },
                    {
                        text: 'Waiting for Approval',
                        value: 'Waiting for Approval',
                    }, {
                        text: 'Waiting for Claiming',
                        value: 'Waiting for Claiming',
                    }],
                onFilter: (value, record) => record.status.status === value,

            }, {
                title: 'Tracker',
                dataIndex: 'status.tracker.fullName',
                filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
                    <div className="custom-filter-dropdown">
                        <Input
                            ref={ele => this.searchInput = ele}
                            placeholder="Search name"
                            value={selectedKeys[0]}
                            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                            onPressEnter={this.filterTrackerSearch(selectedKeys, confirm)}
                        />
                        <Button type="primary" onClick={this.filterTrackerSearch(selectedKeys, confirm)}>Search</Button>
                        <Button onClick={this.filterTrackerReset(clearFilters)}>Reset</Button>
                    </div>
                ),
                filterIcon: filtered => <Icon type="search" style={{color: filtered ? '#108ee9' : '#aaa'}}/>,
                onFilter: (value, record) => record.status.tracker.fullName.toLowerCase().includes(value.toLowerCase()),
                onFilterDropdownVisibleChange: (visible) => {
                    if (visible) {
                        setTimeout(() => {
                            this.searchInput.focus();
                        });
                    }
                },
            }, {
                title: 'Remark',
                dataIndex: 'status.remark',
                width: 200
            },
            {
                title: 'Action', dataIndex: '', key: 'x',
                render: (record, index) =>
                    <div>
                        <a href="javascript:;" onClick={() => this.updateClaim(record, index)}>Update</a>
                        <Divider type="vertical"/>
                        <a href="javascript:;" onClick={() => this.deleteClaim(record, index)}>Delete</a>
                    </div>
            }

        ];
        return (
            <div>
                <Table pagination={{pageSize: 20}} dataSource={this.props.claims}
                       columns={columns} size="small" footer={this.calculateFooter}/>

                <div style={{float: 'right', marginRight: '2%', fontSize: 15, fontStyle: 'oblique'}}>
                    <Upload name="file" action={BaseURL + "import"} showUploadList={false}
                            onChange={() => this.importExcelStarting()}
                            onSuccess={(response) => this.importExcel(response)}
                            disabled={this.state.isImporting}>
                        <Button
                            style={{height: 30}}
                            icon="arrow-up"
                            type="primary">{this.state.isImporting ? `Importing..` : `Import Excel`}</Button>
                    </Upload>
                    <Button
                        style={{marginLeft: "5px"}}
                        icon="export"
                        onClick={() => {
                            exportExcel()
                        }} type="primary">Export to Excel</Button>
                </div>

            </div>
        );
    }

    calculateFooter = (currentPageData) => {
        const totalPHM = currentPageData.reduce((sum, claim) => sum + claim.status.phm, 0);
        const totalPMD = currentPageData.reduce((sum, claim) => sum + claim.status.pmd, 0);
        const totalAMH = currentPageData.reduce((sum, claim) => sum + claim.status.amh, 0);
        return a({totalClaims: currentPageData.length, totalPhm: totalPHM, totalPmd: totalPMD, totalAmh: totalAMH})
    }
}

const a = ({totalClaims, totalPhm, totalPmd, totalAmh}) => (
    <div>
        <span style={{marginRight: '2em'}}>Total Claims: <b>{totalClaims}</b></span>
        <span style={{marginRight: '2em'}}>Total PHM: <b>{totalPhm}</b></span>
        <span style={{marginRight: '2em'}}>Total PMD: <b>{totalPmd}</b></span>
        <span style={{marginRight: '2em'}}>Total AMH: <b>{totalAmh}</b></span>
    </div>
);

const mapStateToProps = state => ({});
const mapDispatchToProps = {switchTab, requestImportedData, requestAllData};
export default connect(mapStateToProps, mapDispatchToProps)(ListClaims);
