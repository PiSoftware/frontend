import React, {Component} from 'react';
import {Input, Button, Select, Tooltip} from 'antd';
import swal from 'sweetalert2';
import {BaseURL} from '../Environment';
import {requestAllData, switchTab} from "../redux/actions";
import connect from "react-redux/es/connect/connect";

const Option = Select.Option;


const initialState = {
    project: {
        id: '',
        siteName: '',
        projectCode: '',
        subProjectCode: '',
        projectType: '',
        approver: null,
        projectLevel: [],
        projectName: null,
        tracker: {id: []},
        active: true
    }
};

class AddProject extends Component {
    constructor(props) {
        super(props);

        this.state = initialState;
        this.onChangeProjectName = this.onChangeProjectName.bind(this);
        this.onProjectSave = this.onProjectSave.bind(this);
        this.clearStatesProject = this.clearStatesProject.bind(this);
        this.onChangeSiteName = this.onChangeSiteName.bind(this);
        this.handleProjectLevelChange = this.handleProjectLevelChange.bind(this);
        this.onChangeProjectType = this.onChangeProjectType.bind(this);
        this.handleTrackerChange = this.handleTrackerChange.bind(this);
        this.onChangeProjectCode = this.onChangeProjectCode.bind(this);
        this.onChangeSubProjectCode = this.onChangeSubProjectCode.bind(this);
        this.onChangeProjectType = this.onChangeProjectType.bind(this);
        this.createProject = this.createProject.bind(this);
        this.editProject = this.editProject.bind(this);
        this.onChangeApprover = this.onChangeApprover.bind(this);
    }

    onProjectSave() {
        if (this.props.selectedProject === undefined || this.props.selectedProject.length == 0) {
            this.createProject();
        } else {
            this.editProject();
        }
    }

    clearStatesProject() {
        this.setState(this.setState(initialState));
        this.props.resetSelectedProjectState();
    }

    createProject() {
        fetch(`${BaseURL}projects`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.project),
        }).then((response) => {
            if (response.status === 200) {
                swal('Added Succesfully', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
                this.props.onSave();
                this.clearStatesProject();
                response.json().then((data) => {

                });
            } else {
                swal('Could not add Project. Please check the fields', {
                    icon: 'error',
                    buttons: false,
                    timer: 1500,
                });
            }
        });
    }

    editProject() {
        const urlParam = this.props.selectedProject.id;
        fetch(`${BaseURL}projects/${urlParam}`, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.project),
        }).then((response) => {
            if (response.status === 200) {
                swal('Updated Succesfully', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
                this.props.onSave();
                this.props.resetSelectedProjectState();
                this.clearStatesProject();
                response.json().then((data) => {
                    // console.log(data);
                });
            } else {
                swal('Could not update Project. Please check the fields', {
                    icon: 'success',
                    buttons: false,
                    timer: 1500,
                });
            }
        });
    }

    onChangeProjectName(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, projectName: val},
            })
        );
    }

    onChangeSiteName(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, siteName: val},
            })
        );
    }

    handleProjectLevelChange(value) {
        const val = value;
        this.setState(prevState => ({
                project: {...prevState.project, projectLevel: val},
            })
        );
    }

    onChangeProjectType(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, projectType: val},
            })
        );
    }

    handleTrackerChange(value) {
        const val = value;
        this.setState(prevState => ({
                project: {...prevState.project, tracker: {id: val}},
            })
        );
    }

    onChangeApprover(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, approver: val},
            })
        );
    }

    onChangeProjectCode(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, projectCode: val},
            })
        );
    }

    onChangeSubProjectCode(e) {
        const val = e.target.value;
        this.setState(prevState => ({
                project: {...prevState.project, subProjectCode: val},
            })
        );
    }

    componentWillMount() {
        if (this.props.selectedProject.length === 0) {
            return;
        }
        this.setState({
            project: {
                projectCode: this.props.selectedProject.projectCode,
                projectLevel: this.props.selectedProject.projectLevel,
                projectName: this.props.selectedProject.projectName,
                projectType: this.props.selectedProject.projectType,
                siteName: this.props.selectedProject.siteName,
                approver: this.props.selectedProject.approver,
                subProjectCode: this.props.selectedProject.subProjectCode,
                tracker: {id: this.props.selectedProject.tracker.id},
            }
        });
    }

    render() {
        return (
            <div>
                <div className="add-project-container">
                    <div>
                        <span style={{fontWeight: 'bold', marginRight: '1em'}}>Site Name</span>
                    </div>
                    <Tooltip placement="right" title={'Rule: <Country - Operator>'}>
                        <Input
                            className="form-input"
                            placeholder="Site Name"
                            value={this.state.project.siteName}
                            onChange={this.onChangeSiteName}
                            style={{width: 400}}
                        />
                    </Tooltip>
                    <h4>Project Name</h4>
                    <Input
                        className="form-input"
                        placeholder="Project Name"
                        value={this.state.project.projectName}
                        onChange={this.onChangeProjectName}
                        style={{width: 400}}
                    />
                    <h4>Tracker</h4>
                    <Select
                        className="form-input"
                        value={this.state.project.tracker.id}
                        showSearch
                        allowClear
                        style={{width: 400}}
                        placeholder="Select a Tracker"
                        optionFilterProp="children"
                        onChange={this.handleTrackerChange}
                    >
                        {this.props.members.map(member => (
                            <Option
                                key={member.id}
                                value={member.id}
                            >
                                {`${member.id} - ${member.fullName}`}
                            </Option>
                        ))}

                    </Select>
                    <h4>Approver</h4>
                    <Input
                        className="form-input"
                        placeholder="Approver"
                        value={this.state.project.approver}
                        onChange={this.onChangeApprover}
                        style={{width: 400}}
                    />
                    <h4>Project Level</h4>
                    <Select
                        className="form-input"
                        placeholder="Project Level"
                        allowClear
                        value={this.state.project.projectLevel}
                        style={{width: 400}}
                        onChange={this.handleProjectLevelChange}
                    >
                        <Option value="Class A">
                            Class A
                        </Option>
                        <Option value="Class B">
                            Class B
                        </Option>
                        <Option value="Class C">
                            Class C
                        </Option>
                    </Select>

                    <div>
                        <span style={{fontWeight: 'bold', marginRight: '1em'}}>Project Type</span>
                    </div>
                    <Tooltip placement="right" title={'Maintenance, Delivery, Pre-sale, etc..'}>
                        <Input
                            className="form-input"
                            placeholder="Project Type"
                            value={this.state.project.projectType}
                            onChange={this.onChangeProjectType}
                            style={{width: 400}}
                        />
                    </Tooltip>
                    <h4>Project Code</h4>
                    <Input
                        className="form-input"
                        placeholder="Project Code"
                        value={this.state.project.projectCode}
                        onChange={this.onChangeProjectCode}
                        style={{width: 400}}
                    />
                    <div>
                        <span style={{fontWeight: 'bold', marginRight: '1em'}}>Sub-Project Code</span>
                    </div>
                    <Tooltip placement="right"
                             title={'Fill with "ProjectCode" if there is no specific sub-project code'}>
                        <Input
                            className="form-input"
                            placeholder="Sub-Project Code"
                            value={this.state.project.subProjectCode}
                            onChange={this.onChangeSubProjectCode}
                            style={{width: 400}}
                        />
                    </Tooltip>
                    <br/>
                    <div className="add-project-buttons" style={{width: 400}}>
                        <Button type="primary" icon="save" onClick={this.onProjectSave}>Save</Button>
                        <Button ghost type="primary" icon="delete" onClick={this.clearStatesProject}>Clear</Button>
                    </div>
                </div>
            </div>

    );
    }
    }

    const mapStateToProps = state => ({});
    const mapDispatchToProps = {switchTab, requestAllData};
    export default connect(mapStateToProps, mapDispatchToProps)(AddProject);