import React, { Component } from 'react';
import { Table, Divider } from 'antd';

export const Columns = [
    {
        title: 'Week',
        dataIndex: 'week',
    }, {
        title: 'Member id',
        dataIndex: 'memberid',
    }, {
        title: 'Full Name',
        dataIndex: 'memberfullname',
    }, {
        title: 'Team',
        dataIndex: 'memberteam',
    }, {
        title: 'Site name',
        dataIndex: 'projectname',
    },
    {
        title: 'PHM',
        dataIndex: 'phm',
    },
    {
        title: 'PMD',
        dataIndex: 'pmd',
    },
    {
        title: 'AMH',
        dataIndex: 'amh',
    },{
        title: 'Transfer By',
        dataIndex: 'transferby',
    },{
        title: 'Status',
        dataIndex: 'status',
    },{
        title: 'Tracker',
        dataIndex: 'tracker',
    },{
        title: 'Remark',
        dataIndex: 'remark',
    },
    { title: 'Action', dataIndex: '', key: 'x',
        render: () =>
            <div>
                <a href="javascript:;" onClick={this.test}>Delete</a>
                <Divider type="vertical" />
                <a href="javascript:;">Update</a>
            </div>
    }

];

export const FilterWeeks = [
    {
        text: 'W1',
        value: 'W1',
    },{
        text: 'W2',
        value: 'W2',
    },{
        text: 'W3',
        value: 'W3',
    },{
        text: 'W4',
        value: 'W4',
    },{
        text: 'W5',
        value: 'W5',
    },{
        text: 'W6',
        value: 'W6',
    },{
        text: 'W7',
        value: 'W7',
    },{
        text: 'W8',
        value: 'W8',
    },{
        text: 'W9',
        value: 'W9',
    },{
        text: 'W10',
        value: 'W10',
    },{
        text: 'W11',
        value: 'W11',
    },{
        text: 'W12',
        value: 'W12',
    },{
        text: 'W13',
        value: 'W13',
    },{
        text: 'W14',
        value: 'W14',
    },{
        text: 'W15',
        value: 'W15',
    },{
        text: 'W16',
        value: 'W16',
    },{
        text: 'W17',
        value: 'W17',
    },{
        text: 'W18',
        value: 'W18',
    },{
        text: 'W19',
        value: 'W19',
    },{
        text: 'W20',
        value: 'W20',
    },{
        text: 'W21',
        value: 'W21',
    },{
        text: 'W22',
        value: 'W22',
    },{
        text: 'W23',
        value: 'W23',
    },{
        text: 'W24',
        value: 'W24',
    },{
        text: 'W25',
        value: 'W25',
    },{
        text: 'W26',
        value: 'W26',
    },{
        text: 'W27',
        value: 'W27',
    },{
        text: 'W28',
        value: 'W28',
    },{
        text: 'W29',
        value: 'W29',
    },{
        text: 'W30',
        value: 'W30',
    },{
        text: 'W31',
        value: 'W31',
    },{
        text: 'W32',
        value: 'W32',
    },{
        text: 'W33',
        value: 'W33',
    },{
        text: 'W34',
        value: 'W34',
    },{
        text: 'W35',
        value: 'W35',
    },{
        text: 'W36',
        value: 'W36',
    },{
        text: 'W37',
        value: 'W37',
    },{
        text: 'W38',
        value: 'W38',
    },{
        text: 'W39',
        value: 'W39',
    },{
        text: 'W40',
        value: 'W40',
    },{
        text: 'W41',
        value: 'W41',
    },{
        text: 'W42',
        value: 'W42',
    },{
        text: 'W43',
        value: 'W43',
    },{
        text: 'W44',
        value: 'W44',
    },{
        text: 'W45',
        value: 'W45',
    },{
        text: 'W46',
        value: 'W46',
    },{
        text: 'W47',
        value: 'W47',
    },{
        text: 'W48',
        value: 'W48',
    },{
        text: 'W49',
        value: 'W49',
    },{
        text: 'W50',
        value: 'W50',
    },{
        text: 'W51',
        value: 'W51',
    },{
        text: 'W52',
        value: 'W52',
    },{
        text: 'W53',
        value: 'W53',
    }
];

export const Weeks = [
    {
        key: '1',
        value: 'W1',
    },{
        key: '2',
        value: 'W2',
    },{
        key: '3',
        value: 'W3',
    },{
        key: '4',
        value: 'W4',
    },{
        key: '5',
        value: 'W5',
    },{
        key: '6',
        value: 'W6',
    },{
        key: '7',
        value: 'W7',
    },{
        key: '8',
        value: 'W8',
    },{
        key: '9',
        value: 'W9',
    },{
        key: '10',
        value: 'W10',
    },{
        key: '11',
        value: 'W11',
    },{
        key: '12',
        value: 'W12',
    },{
        key: '13',
        value: 'W13',
    },{
        key: '14',
        value: 'W14',
    },{
        key: '15',
        value: 'W15',
    },{
        key: '16',
        value: 'W16',
    },{
        key: '17',
        value: 'W17',
    },{
        key: '18',
        value: 'W18',
    },{
        key: '19',
        value: 'W19',
    },{
        key: '20',
        value: 'W20',
    },{
        key: '21',
        value: 'W21',
    },{
        key: '22',
        value: 'W22',
    },{
        key: '23',
        value: 'W23',
    },{
        key: '24',
        value: 'W24',
    },{
        key: '25',
        value: 'W25',
    },{
        key: '26',
        value: 'W26',
    },{
        key: '27',
        value: 'W27',
    },{
        key: '28',
        value: 'W28',
    },{
        key: '29',
        value: 'W29',
    },{
        key: '30',
        value: 'W30',
    },{
        key: '31',
        value: 'W31',
    },{
        key: '32',
        value: 'W32',
    },{
        key: '33',
        value: 'W33',
    },{
        key: '34',
        value: 'W34',
    },{
        key: '35',
        value: 'W35',
    },{
        key: '36',
        value: 'W36',
    },{
        key: '37',
        value: 'W37',
    },{
        key: '38',
        value: 'W38',
    },{
        key: '39',
        value: 'W39',
    },{
        key: '40',
        value: 'W40',
    },{
        key: '41',
        value: 'W41',
    },{
        key: '42',
        value: 'W42',
    },{
        key: '43',
        value: 'W43',
    },{
        key: '44',
        value: 'W44',
    },{
        key: '45',
        value: 'W45',
    },{
        key: '46',
        value: 'W46',
    },{
        key: '47',
        value: 'W47',
    },{
        key: '48',
        value: 'W48',
    },{
        key: '49',
        value: 'W49',
    },{
        key: '50',
        value: 'W50',
    },{
        key: '51',
        value: 'W51',
    },{
        key: '52',
        value: 'W52',
    },{
        key: '53',
        value: 'W53',
    }
];