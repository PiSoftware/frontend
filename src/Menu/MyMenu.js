import React from 'react';
import { connect } from 'react-redux';

import { Menu, Icon } from 'antd';
import 'antd/dist/antd.css';

import { switchTab } from '../redux/actions';
import { getActiveTab } from '../redux/selectors';


const MyMenu = ({ onMenuClick, current }) => (
    <Menu
        onClick={(e) => {
            onMenuClick(e.key);
        }}
        selectedKeys={[current]}
        mode="horizontal"
        theme="dark"
    >
        <Menu.Item key="claims">
            <Icon type="home" />
            List Claims
        </Menu.Item>
        <Menu.Item key="claim-add">
            <Icon type="edit" />
            Add Claims
        </Menu.Item>
        <Menu.Item key="members">
            <Icon type="user" />
            List Members
        </Menu.Item>
        <Menu.Item key="member-add">
            <Icon type="user-add" />
            Add Members
        </Menu.Item>
        <Menu.Item key="projects">
            <Icon type="table" />
            List Projects
        </Menu.Item>
        <Menu.Item key="project-add">
            <Icon type="plus" />
            Add Projects
        </Menu.Item>
        <Menu.Item key="imported">
            <Icon type="arrow-up" />
            Import Logs
        </Menu.Item>
        <Menu.Item key="charts" disabled>
            <Icon type="pie-chart" />
            Claim Charts
        </Menu.Item>
    </Menu>
);

const mapStateToProps = state => ({
    getActiveTab: getActiveTab(state),
});

const mapDispatchToProps = { switchTab };
export default connect(mapStateToProps, mapDispatchToProps)(MyMenu);
