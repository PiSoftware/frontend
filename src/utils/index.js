import moment from "moment";
import swal from 'sweetalert2';

export const setDateInterval = (week) => {
    const thisYear = (new Date()).getFullYear();
    const startYear = new Date(`1/1/${thisYear}`);
    const defaultStart = moment(startYear.valueOf());

    // var thenum = (week).replace(/^\D+/g, '');
    const date = moment(defaultStart).add(week, 'weeks').startOf('isoweek').format('DD.MM.YYYY');
    const start = moment().day('Monday').week(week).format('DD.MM.YYYY');

    const end = moment(start, 'DD.MM.YYYY').add(6, 'days').format('DD.MM.YYYY');
    const dateStr = `${start} - ${end}`;
    return (dateStr);
};


export const deleteConfirm = () => {
    return false
}
export const deleteConfirmation = (callback) => {
    //return window.confirm("Are you sure you want to delete this item?")
    swal({
        title: 'Are you sure?',
        text: "Are you sure you want to delete this item?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            callback()
        }
    })
};