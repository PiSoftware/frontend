import {BaseURL} from "../Environment";

export const fetchData = async () => {
    try {
        const response = await fetch(BaseURL + "all");
        const data = await response.json();
        return data;
    } catch (e) {
        console.log(e);
    }
};

